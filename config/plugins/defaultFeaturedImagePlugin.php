<?php
return [
    'settingPage' => [
        'pageTitle' => 'Set Default Featured Image',
        'menuTitle' => 'Set Default Featured Image',
        'capability' => 'manage_options',
        'menuSlug' => 'default-featured-image',
        'template' => 'defaultFeaturedImage'
    ],
    'registerSettings' => [
        'optionGroup' => 'defaultFeaturedImages',
        'options'     => [
            'defaultFeaturedImage' => [],
        ]
    ]
];