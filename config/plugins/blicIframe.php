<?php
return [
	'settingPage'      => [
		'pageTitle'  => 'Blic Iframe',
		'menuTitle'  => 'Blic Iframe',
		'capability' => 'manage_options',
		'menuSlug'   => 'blic-iframe',
		'template'   => 'blicIframe'
	],
	'registerSettings' => [
		'optionGroup' => 'blicIframeIds',
		'options'     => [
			'blicIframeIds' => [],
		]
	]
];