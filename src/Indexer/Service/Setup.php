<?php

namespace GfWpPluginContainer\Indexer\Service;

use Elastica\Client;
use Elastica\Type\Mapping;
use Elastica\Response;
use GfWpPluginContainer\Indexer\Config\ConfigInterface as Config;

class Setup
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Config
     */
    private $config;

    /**
     * Setup constructor.
     * @param Client $elasticaClient
     * @param Config $config
     */
    public function __construct(Client $elasticaClient, Config $config)
    {
        $this->client = $elasticaClient;
        $this->config = $config;
    }

    /**
     * @param bool $recreate
     */
    public function createIndex($recreate = false)
    {
        if ($recreate) {
            try {
                $this->client->getIndex($this->config->getIndex())->delete();
            } catch (\Exception $e) {
                if (!strstr($e->getMessage(), 'no such index')) {
                    var_dump($e->getMessage());
                }
            }
        }
        $this->setupAnalyzers();
        $response = $this->setupMapping();
        if (!$response->isOk()) {
            throw new \Exception($response->getError());
        }
        $msg = $this->config->getIndex() . ' index created.' . PHP_EOL;
        if ($recreate) {
            $msg = $this->config->getIndex() . ' index recreated.' . PHP_EOL;
        }

        return $msg;
    }

    /**
     * @return Response
     */
    private function setupMapping()
    {
//        $mapping = new Mapping($this->client->getIndex('article')->getType('article'));
        //$this->config->getMapping()
        $type = $this->client->getIndex($this->config->getIndex())->getType($this->config->getType());
        $mapping = new Mapping();
        $mapping->setType($type);
        $mapping->setProperties($this->config->getMapping());

        return $mapping->send();
    }

    /**
     * @return void
     */
    private function setupAnalyzers()
    {
        $this->client->getIndex($this->config->getIndex())->create($this->config->getSetupConfig());
    }
}