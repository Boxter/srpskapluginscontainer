<?php

namespace GfWpPluginContainer\Indexer\Factory;

use GfWpPluginContainer\Indexer\Factory\ElasticClientFactory as ElasticFactory;

class ArticleSetupFactory
{
    /**
     * @var ElasticClientFactory
     */
    private $elasticClientFactory;

    /**
     * ProductSetupFactory constructor.
     */
    public function __construct(ElasticFactory $elasticClientFactory)
    {
        $this->elasticClientFactory = $elasticClientFactory;
    }

    /**
     * @return \GfWpPluginContainer\Indexer\Service\Setup
     */
    public function make()
    {
        return new \GfWpPluginContainer\Indexer\Service\Setup(
            ElasticFactory::make(),
            new \GfWpPluginContainer\Indexer\Config\Article()
        );
    }
}