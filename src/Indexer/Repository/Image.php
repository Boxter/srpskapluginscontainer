<?php

namespace GfWpPluginContainer\Indexer\Repository;


class Image
{
    public static function elasticImageSearch($input, $perPage = PER_PAGE, $currentPage = 1)
    {
        $mapper = \GfWpPluginContainer\Indexer\Factory\Mapper::make(ES_INDEX_IMAGE);
        $mapper->search($input, $perPage, $currentPage);
        $images = [];
        foreach ($mapper->getResultSet() as $result) {
            $images[] = $result->getData();
        }

        return ['totalCount' => $mapper->getResultSet()->getTotalHits(), 'images' => $images];
    }

    public static function syncImageToElastic(\WP_Post $post) {
        $indexer = new \GfWpPluginContainer\Indexer\Service\Indexer(
            \GfWpPluginContainer\Indexer\Factory\ElasticClientFactory::make()->getIndex(ES_INDEX_IMAGE)
        );
        $indexer->indexImage($post);
    }
}