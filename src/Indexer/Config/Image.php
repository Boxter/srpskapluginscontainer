<?php

namespace GfWpPluginContainer\Indexer\Config;

class Image implements ConfigInterface
{
    private $type = ES_INDEX_IMAGE;

    private $index = ES_INDEX_IMAGE;

    private $setupConfig = [
        'settings' => [
            'number_of_shards' => 3,
            'number_of_replicas' => 1,
            'analysis' => [
                'analyzer' => array(
                    'default' => array(
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => array('lowercase', 'stop', 'trim', 'asciifolding') // 'custom_ascii_folding'
                    ),
                    'search' => array(
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => array('lowercase', 'trim', 'asciifolding') // 'custom_ascii_folding' @TODO install icu_folding
                    )
                ),
//                'filter' => array(
//                    'mySnowball' => array(
//                        'type' => 'snowball',
//                        'language' => 'German'
//                    ),
//                    'custom_ascii_folding' => array(
//                        'type' => 'asciifolding',
//                        'preserve_original' => true
//                    ),
//                    'test' => array(
//                        'type' => 'stemmer',
//                        'language' => 'Russian'
//                    ),
//                )
            ]
        ]
    ];

    private $mapping = [
        'entity' => [
            'properties' => [
                'postId' => array('type' => 'integer'),
                'author' => array(
                    'type' => 'nested',
                    'properties' => array(
                        'id' => array('type' => 'integer'),
                        'name' => array('type' => 'text'),
                        'url' => array('type' => 'text'),
                    ),
                ),
                'title' => array('type' => 'text', 'fielddata' => true),
                'createdAt' => array('type' => 'date'),
                'potpis' => array('type' => 'text'),
                'legenda' => array('type' => 'text'),
                'synced' => array('type' => 'integer'),
                'filename' => array('type' => 'integer'),
            ]
        ],
    ];

    public function getMapping()
    {
        return $this->mapping;
    }

    public function getSetupConfig()
    {
        return $this->setupConfig;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getIndex()
    {
        return $this->index;
    }
}