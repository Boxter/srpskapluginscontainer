<?php


namespace GfWpPluginContainer\Pagination;


use GfWpPluginContainer\GfShopThemePlugins;

class Pagination
{
    public static function dottedPagination($maxPage, $currentPage) {
        if (wp_is_mobile()) {
            GfShopThemePlugins::getTemplatePartials('pagination', 'pagination', 'dottedPaginationMobile',
                ['lastPage' => $maxPage, 'page' => $currentPage]);
        } else {
            GfShopThemePlugins::getTemplatePartials('pagination', 'pagination', 'dottedPagination',
                ['lastPage' => $maxPage, 'page' => $currentPage]);
        }
    }
}