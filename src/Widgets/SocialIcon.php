<?php


namespace GfWpPluginContainer\Widgets;


class SocialIcon extends \WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'gf_SocialIcon',
            'Social Icon',
            ['description' => 'Widget for showing selected social icon']
        );

    }


    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        $iconUrl = '';
        $urlText = '';

        if (isset($instance['iconUrl']) && strlen($instance['iconUrl']) > 0) {
            $iconUrl = $instance['iconUrl'];
        }
        if(isset($instance['urlText']) && strlen($instance['urlText']) > 0) {
            $urlText = $instance['urlText'];
        }
        $selectedYoutube = '';
        $selectedFacebook = '';
        $selectedInstagram = '';
        $selectedTwitter = '';

        if (isset($instance['iconSelect'])) {
            switch ($instance['iconSelect']) {
                case 'facebook':
                    $selectedFacebook = 'selected';
                    break;
                case 'twitter':
                    $selectedTwitter = 'selected';
                    break;
                case 'instagram':
                    $selectedInstagram = 'selected';
                    break;
                case 'youtube':
                    $selectedYoutube = 'selected';
                    break;
            }
        }

        ?>
        <label for="<?= $this->get_field_id('iconSelect') ?>">Select the icon you wish to use</label>
        <div>
            <select id="<?= $this->get_field_id('iconSelect') ?>" name=<?= $this->get_field_name('iconSelect') ?>>
                <option <?= $selectedFacebook ?> value="facebook">Facebook</option>
                <option <?= $selectedTwitter ?> value="twitter">Twitter</option>
                <option <?= $selectedInstagram ?> value="instagram">Instagram</option>
                <option <?= $selectedYoutube ?> value="youtube">Youtube</option>
            </select>
        </div>
        <label for="<?= $this->get_field_id('iconUrl') ?>">Insert url for the icon</label>
        <div>
            <input type="url" id="<?= $this->get_field_id('iconUrl') ?>"
                   name="<?= $this->get_field_name('iconUrl') ?>" value="<?= $iconUrl ?>"
                   placeholder="http://">
        </div>
        <div>
            <label for="<?= $this->get_field_id('urlText') ?>">Insert text next to the icon (optional)</label>
            <input type="text" id="<?= $this->get_field_id('urlText') ?>"
                   name="<?= $this->get_field_name('urlText') ?>" value="<?= $urlText ?>"
                   placeholder="Enter the text you want to display next to the icon">
        </div>

        <?php
    }

    /**
     * Front-end display of widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     * @see WP_Widget::widget()
     *
     */

    public function widget($args, $instance)
    {
        // if its mobile
        if (wp_is_mobile()) {
            $iconClass = isAmp()? 'fa' : 'fab';
            // if the user set the urlText(text to be displayed next to the icon)
            if (!isset($instance['urlText']) && isset($instance['iconUrl']) && isset($instance['iconSelect']) && strlen($instance['iconUrl']) > 0) {
                echo $args['before_widget'];
                echo '<a style="color:#212121;" href="' . $instance['iconUrl'] . '" target="_blank" aria-label="' . $instance['iconSelect'] .'"><i class="'.$iconClass.' fa-'
                    . $instance['iconSelect']
                    . ($instance['iconSelect'] === 'facebook' ? '-f' : '')
                    . '"></i></a>';
                echo $args['after_widget'];
                return;
            }
            // if the user didn't set the urlText(text to be displayed next to the icon)
            echo $args['before_widget'];
            echo '<a style="color:#212121;" href="' . $instance['iconUrl'] . '" target="_blank" aria-label="' . $instance['iconSelect'] .'"><span><i class="'.$iconClass.' fa-'
                . $instance['iconSelect']
                . ($instance['iconSelect'] === 'facebook' ? '-f' : '')
                . '"></i></span>' . $instance['urlText'] . '</a>';
            echo $args['after_widget'];
        }
        // if its desktop / else
        else {
            // if the user set the urlText(text to be displayed next to the icon)
            if (!isset($instance['urlText']) && isset($instance['iconUrl']) && isset($instance['iconSelect']) && strlen($instance['iconUrl']) > 0) {
                echo $args['before_widget'];
                echo '<a href="' . $instance['iconUrl'] . '" target="_blank" aria-label="' . $instance['iconSelect'] .'"><i class="fa fa-' . $instance['iconSelect'] . '"></i></a>';
                echo $args['after_widget'];
                return;
            }
            // if the user didn't set the urlText(text to be displayed next to the icon)
            echo $args['before_widget'];
            echo '<a href="' . $instance['iconUrl'] . '" target="_blank" aria-label="' . $instance['iconSelect'] .'"><span><i class="fa fa-' . $instance['iconSelect'] . '"></i></span>' . $instance['urlText'] . '</a>';
            echo $args['after_widget'];
        }
    }


    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     * @see WP_Widget::update()
     *
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['iconSelect'] = (!empty($new_instance['iconSelect'])) ? $new_instance['iconSelect'] : '';
        $instance['iconUrl'] = (!empty($new_instance['iconUrl'])) ? $new_instance['iconUrl'] : '';
        $instance['urlText'] = (!empty($new_instance['urlText'])) ? $new_instance['urlText'] : '';
        return $instance;
    }

}