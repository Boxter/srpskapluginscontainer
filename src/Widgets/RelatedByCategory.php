<?php

namespace GfWpPluginContainer\Widgets;

use GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner;
use GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors;
use GfWpPluginContainer\Wp\PostHelper;


class RelatedByCategory extends \WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'gf_related_category',
            'Related posts by category',
            ['description' => 'Widget for displaying related posts by category']
        );
    }

    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        // Default value of 4
        $numberOfPosts = '4';
        // If there is input
        if (isset($instance['numberOfPosts']) && $instance['numberOfPosts'] !== '') {
            $numberOfPosts = $instance['numberOfPosts'];
        }
        ?>
        <label for="<?= $this->get_field_id('numberOfPosts') ?>">Izaberite broj tekstova za prikaz (4,8,12)</label>

        <div>
            <input type="text" id="<?= $this->get_field_id('numberOfPosts') ?>"
                   name="<?= $this->get_field_name('numberOfPosts') ?>" value="<?= $numberOfPosts ?>">
        </div>
        <?php
    }

    public function widget($args, $instance)
    {
        global $cache, $isApp, $appSinglePost;
        if($isApp) {
            $key = 'relatedByCategoryApp#' . $appSinglePost->ID;
        } else {
            $key = 'relatedByCategory#' . get_queried_object_id();
        }

        $html = $cache->get($key);
        if ($html === false) {
            $html = $this->prepareHtml($args, $instance);
            $cache->set($key, $html);
        }

        echo $html;
    }

    public function prepareHtml($args, $instance)
    {
        $multipleAuthors = new MultipleAuthors();
        global $restrictedPosts, $isApp, $appSinglePost;
        // If the user wants to display other than 4,8,12 posts reset to 4
        if (!in_array($instance['numberOfPosts'], [4, 8, 12])) {
            $instance['numberOfPosts'] = 4;
        }

        if ($isApp) {
            $currentPostId = $appSinglePost->ID;
        } else {
            $currentPostId = get_the_ID();
        }
        $category = get_the_category($currentPostId)[0]->term_id;
        if (!in_array($currentPostId, $restrictedPosts)) {
            $restrictedPosts[] = $currentPostId;
        }
        // Get posts with the same category
        $relatedPostsCategory = get_posts([
            'category' => $category,
            'numberposts' => $instance['numberOfPosts'],
            'exclude' => $restrictedPosts,
            'post_status' => 'publish',
            'category__not_in' => [55687],
            'date_query'    => array(
                'column'  => 'post_date',
                'after'   => '-30 days'
            )
        ]);
        $html = '<div class="container"> <!-- Related Posts by Category Start -->
        <div class="box">
        <h3 class="box__title">Izdvajamo</h3>
        <section class="news">';
        foreach ($relatedPostsCategory as $post) {
            $restrictedPosts[] = $post->ID;
            $category = get_the_category($post->ID)[0];
            $categoryLink = $isApp ?
                parseAppUrl('page', str_replace('category/', '', get_category_link($category->term_id))) :
                str_replace('category/', '', get_category_link($category->term_id)) ;
            $categoryName = $category->name;
            $postTitle = $post->post_title;
            $postType = $post->post_type;
            $postPermalink = $isApp ? parseAppUrl('article',get_permalink($post->ID)): get_permalink($post->ID) ;
            $postThumbnailUrl = get_the_post_thumbnail_url($post->ID, 'list-small');
            if(!$postThumbnailUrl) {
                $postThumbnailUrl=wp_get_attachment_image_url(get_option('defaultFeaturedImage'),'list-small');
            }
            if (mb_strtoupper($categoryName) === 'KOLUMNE' ){
                /* @var GfPostOwner $owner */
                $owner = $multipleAuthors->getOwnersForPost($post->ID)[0];
                $categoryName = $owner->getAuthorDisplayName();
                $authorUrl =  get_author_posts_url($owner->getAuthorId());
                $categoryLink = $isApp ? parseAppUrl('author',$authorUrl) : $authorUrl;
            }
            if(function_exists( 'is_amp_endpoint' ) && is_amp_endpoint()){
                $html .= include( __DIR__ . '/../../templates/article/listItemMobileAmp.phtml' );
            }
            else {
                $html .= include(__DIR__ . '/../../templates/article/listItem.phtml');
            }

        }
        $html .= PHP_EOL . '</section>
            </div>
        </div><!-- Related Posts by Category End -->';

        return $html;
    }
}