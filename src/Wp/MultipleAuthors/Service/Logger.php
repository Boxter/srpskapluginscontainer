<?php
namespace GfWpPluginContainer\Wp\MultipleAuthors\Service;

use GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner;
use wpdb;

class Logger
{

    private $db;
    /**
     * @var string
     */
    private $tableName;

    /**
     * PostOwnerLogger constructor.
     * @param wpdb $db
     */
    public function __construct(wpdb $db)
    {
        $this->db = $db;
        $this->tableName = $this->db->prefix . 'gfMultipleAuthorsChangeLog';
    }

    public function createLogTable(): void
    {
        $charset = $this->db->get_charset_collate();
        $sql = "CREATE TABLE {$this->tableName}(
	        id int auto_increment,
	        action varchar(64) not null,
	        postId bigint(20) not null,
	        oldData blob,
	        newData blob not null,
	        userId bigint(20) not null,
	        createdAt TIMESTAMP default CURRENT_TIMESTAMP not null,
		    primary key (id)
            ){$charset}";
        $this->db->query($sql);
    }

    private function logUpdate(int $postId, array $newData, array $oldData, int $userId): void
    {
        $serializedNewData = serialize($newData);
        $serializedOldData = serialize($oldData);
        $sql = "INSERT INTO {$this->tableName} (`action`, `postId`,`oldData`, `newData`, `userId`) VALUES 
                                               ('update', {$postId},'{$serializedOldData}', '{$serializedNewData}',{$userId});";
        $this->db->query($sql);
    }

    private function logCreate(int $postId, $data, int $userId): void
    {
        $serializedData = serialize($data);
        $sql = "INSERT INTO {$this->tableName} (`action`, `postId`, `newData`, `userId`) VALUES 
                                               ('create', {$postId}, '{$serializedData}',{$userId});";
        $this->db->query($sql);
    }

    public function log(string $action,int $userId, int $postId, array $newData, $oldData = []): void
    {
        if ($action === 'create'){
            $this->logCreate($postId, $newData, $userId);
        }
        if ($action === 'update'){
            $this->logUpdate($postId, $newData, $oldData, $userId);
        }

    }
}