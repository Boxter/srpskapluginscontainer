<?php


namespace GfWpPluginContainer\Wp\MultipleAuthors\Mapper;

use GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner;

class MultipleAuthors
{
    /**
     * @var \wpdb
     */
    private $db;
    /**
     * @var string
     */
    private $tableName;

    /**
     * MultipleAuthors constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db;
        $this->tableName = $this->db->prefix . 'gfPostOwners';
    }

    public function insert(GfPostOwner $model)
    {
        $sql = "INSERT INTO {$this->tableName} (`postId`, `authorId`, `authorDisplayName`)
            VALUES ({$model->getPostId()}, {$model->getAuthorId()}, '{$model->getAuthorDisplayName()}') ";
        $this->db->query($sql);
    }

    public function delete(int $postId)
    {
        $sql = "DELETE FROM {$this->tableName} WHERE `postId` = {$postId} ";
        $this->db->query($sql);
    }

    public function fetchPostIdsForOwner(int $userId)
    {
        $postsTable = $this->db->prefix . 'posts';
        $sql = "SELECT postId FROM {$this->tableName} a JOIN {$postsTable} b ON (a.postId = b.ID) WHERE a.authorId = {$userId} AND b.post_status <> 'trash';";
        return $this->db->get_results($sql, ARRAY_N) ?? [];
    }

    public function fetchCoreAuthorPostIds($userId)
    {
        $sql = "SELECT ID FROM wp_posts WHERE post_author = {$userId} AND post_type = 'post';";
        return $this->db->get_results($sql, ARRAY_N) ?? [];
    }

    public function fetchOwnersForPost(int $postId)
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE `postId` = {$postId}";
        return $this->db->get_results($sql, ARRAY_A) ?? [];
    }

    public function fetchOwnerIdsForPost(int $postId)
    {
        $sql = "SELECT `authorId` FROM {$this->tableName} WHERE `postId` = {$postId}";
        return $this->db->get_results($sql, ARRAY_N);
    }

    public function fetchNumberOfPostsForAuthor($authorId)
    {
        $sql = "SELECT COUNT(postId) FROM {$this->tableName} WHERE `authorId` = {$authorId}";
        return $this->db->get_var($sql);
    }
}