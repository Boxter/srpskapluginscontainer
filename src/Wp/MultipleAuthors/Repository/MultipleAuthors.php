<?php


namespace GfWpPluginContainer\Wp\MultipleAuthors\Repository;


use GfWpPluginContainer\Wp\MultipleAuthors\Mapper\MultipleAuthors as Mapper;
use GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner;

class MultipleAuthors
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * MultipleAuthors constructor.
     * @param Mapper $mapper
     */
    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    public function insert($data)
    {
        $this->mapper->insert($this->make($data));
    }

    public function delete(int $postId)
    {
        $this->mapper->delete($postId);
    }

    public function getPostIdsForAuthor(int $authorId)
    {
        $data = $this->mapper->fetchPostIdsForOwner($authorId);
        $formattedData = [];
        foreach ($data as $key => $value){
            $formattedData[] = $value[0];
        }
        return $formattedData;
    }

    public function getCoreAuthorPostIds($userId)
    {
        $data =  $this->mapper->fetchCoreAuthorPostIds($userId);
        $formattedData = [];
        foreach ($data as $key => $value){
            $formattedData[] = $value[0];
        }
        return $formattedData;
    }

    public function getNumberOfPostsForAuthor($authorId)
    {
        $data = $this->mapper->fetchNumberOfPostsForAuthor($authorId);
//        $formattedData = [];
//        foreach ($data as $key => $value){
//            $formattedData[] = $value[0];
//        }
        return $data;
    }

    public function getOwnersForPost(int $postId, $returnType = null)
    {
        $owners = $this->mapper->fetchOwnersForPost($postId);
        if (count($owners) > 0){
            if (!$returnType) {
                $data = [];
                foreach ($owners as $postOwner) {
                    $data[] = $this->make($postOwner);
                }
                return $data;
            }

            return $owners;
        }
        $data = [];
        $post = get_post($postId);
        $coreAuthor = [
            'postId' => $postId,
            'authorId' => $post->post_author,
            'authorDisplayName' => get_the_author_meta('display_name', $post->post_author)
        ];
        if (!$returnType) {
            $data[] = $this->make($coreAuthor);
            return $data;
        }
        $data[] = $coreAuthor;

        return $data;
    }

    public function getOwnerIdsForPost(int $postId)
    {
        $data = $this->mapper->fetchOwnerIdsForPost($postId);
        $formattedData = [];
        foreach ($data as $key => $value){
            $formattedData[] = (int)$value[0];
        }
        return $formattedData;
    }

    private function make($data): GfPostOwner
    {
        $postId = (int)$data['postId'];
        $authorId = (int)$data['authorId'];
        $authorDisplayName = $data['authorDisplayName'];
        $id = $data['id'] ?? null;
        $createdAt = $data['createdAt'] ?? 0;
        $updatedAt = $data['updatedAt'] ?? 0;
        return new GfPostOwner($id, $postId, $authorId, $authorDisplayName);
    }
}