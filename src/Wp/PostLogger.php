<?php


namespace GfWpPluginContainer\Wp;

/**
 * Class PostLogger
 * @package GfWpPluginContainer\Wp
 * @todo save tag changes
 * @todo figure out how to save backups and autosaves
 */
class PostLogger
{
    /**
     * @var \QM_DB|\wpdb
     */
    private $db;
    /**
     * @var string
     */
    private $tableName;

    /**
     *
     */
    public function init()
    {
        add_filter('update_post_meta', [$this, 'detectChangeInThumbnail'], 99, 4);
        add_action('pre_post_update', [$this, 'detectAuthorChange'],98,2);
        add_action('pre_post_update', [$this, 'detectStatusChange'],99,2);
        global $wpdb;
        $this->db = $wpdb;
        $this->tableName = $this->db->prefix.'postsLog';
//        $this->createDatabaseTable();
    }

    /**
     * Used only once @todo create some kind of activate hook for this stuff
     */
    private function createDatabaseTable()
    {
        $charsetCollate = $this->db->get_charset_collate();
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $tableName = $this->db->prefix . 'postsLog';
        $sql = "CREATE TABLE $this->tableName (
                logId INT(10) NOT NULL AUTO_INCREMENT,
                postId INT(10) NOT NULL,
                attribute VARCHAR(256) NOT NULL,
                value VARCHAR(256) NOT NULL,
                createdBy INT(10) NOT NULL,
                createdAt DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (logId)
                ) $charsetCollate;";
        dbDelta($sql);
    }

    /**
     * @param $metaId
     * @param $postId
     * @param $metaKey
     * @param $metaValue
     */
    public function detectChangeInThumbnail($metaId, $postId, $metaKey, $metaValue)
    {

        $userId = get_current_user_id();
        if ($metaKey === '_thumbnail_id') {
            $lastEdit = $this->getNewestLogByAttribute($postId, 'featuredImage');
            if (!$lastEdit || $lastEdit['value'] !== (string)$metaValue) {
                $imageUrl = wp_get_attachment_url($metaValue);
                $this->createLog((int)$postId, $userId,'featuredImage', $imageUrl);
            }
        }
    }

    /**
     * @param $postId
     * @param $postData
     */
    public function detectAuthorChange($postId, $postData)
    {
        if ($postData['post_mime_type'] === '' && $postData['post_type'] === 'post') {
            $userId = get_current_user_id();
            $author = $postData['post_author'];
            $lastEdit = $this->getNewestLogByAttribute($postId, 'author');
            if (!$lastEdit || $lastEdit['value'] != $author) {
                $this->createLog((int)$postId, $userId, 'author', (int)$author);
            }
        }
    }

    /**
     * @param $postId
     * @param $postData
     */
    public function detectStatusChange($postId, $postData)
    {
        if ($postData['post_mime_type'] === '' && $postData['post_type'] === 'post') {
            $userId = get_current_user_id();
            $status = $postData['post_status'];
            $lastEdit = $this->getNewestLogByAttribute($postId, 'status');
            if (!$lastEdit || $lastEdit['value'] !== $status) {
                $this->createLog((int)$postId, $userId, 'status', $status);
            }
        }
    }

    /**
     * @param $postId
     * @param $userId
     * @param $attribute
     * @param $value
     */
    public function createLog($postId, $userId, $attribute, $value)
    {
        $this->db->insert($this->tableName, [
            'postId' => $postId,
            'createdBy' => $userId,
            'attribute' => $attribute,
            'value' => $value
        ]);
    }

    /**
     * @param $postId
     */
    public function getLogsForPost($postId)
    {
        $query = $this->db->get_results("SELECT * FROM $this->tableName WHERE `postId` LIKE $postId");
    }

    /**
     * @param $postId
     * @param $attribute
     * @return mixed
     */
    public function getNewestLogByAttribute($postId, $attribute)
    {
       return $this->db->get_results("SELECT * FROM $this->tableName WHERE `postId` = $postId AND `attribute` = '$attribute' ORDER BY `createdAt` DESC",ARRAY_A)[0];
    }
}