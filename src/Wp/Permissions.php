<?php


namespace GfWpPluginContainer\Wp;


class Permissions
{
    //This should be hooked in main plugin activation
    public function activate()
    {
//        add_action('init', [$this,'specialUserPermissions']);
//        add_action('init', [$this, 'addCapsToEditors']);
    }

    public function specialUserPermissions()
    {
        $user1WithUserControl = get_user_by('id', 7); // Boris Lakić
        $user1WithUserControl->add_cap('list_users');
        $user1WithUserControl->add_cap('create_users');
        $user1WithUserControl->add_cap('edit_users');
        $user1WithUserControl->add_cap('promote_users');

        $user2WithUserControl = get_user_by('id', 8); // Branko Tomić
        $user2WithUserControl->add_cap('list_users');
        $user2WithUserControl->add_cap('create_users');
        $user2WithUserControl->add_cap('edit_users');
        $user2WithUserControl->add_cap('promote_users');
    }

    public function addCapsToEditors()
    {
        $editorRole = get_role('editor');
        $editorRole->add_cap('edit_theme_options');
        $editorRole->add_cap('manage_options');
    }
}