<?php


namespace GfWpPluginContainer\Gutenberg\Blocks\GalleryBlock;


class GalleryBlock
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public function register()
    {
        register_block_type('wpplugincontainer/gfgalleryblock', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));

    }


    public function render($attributes, $content)
    {
        if(wp_is_mobile() && function_exists( 'is_amp_endpoint' ) && is_amp_endpoint())
            return '';

        $html = '<div class="carouselDiv" style="width:90%;margin:auto;">';
        foreach ($attributes['images'] as $image) {
            $html .='<img src="' . $image['url'] . '" style="width:100%; height:100%;"/>';
        }
        $html .= '</div>';
        return $html;
    }
}