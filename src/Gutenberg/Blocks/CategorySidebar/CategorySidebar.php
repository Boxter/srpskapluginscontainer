<?php

namespace GfWpPluginContainer\Gutenberg\Blocks\CategorySidebar;

use GfWpPluginContainer\GfShopThemePlugins;
use GfWpPluginContainer\Gutenberg\Blocks\BaseBlock\AbstractBlock;
use GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors;

class CategorySidebar extends AbstractBlock
{
    private function getPostsForTemplate($attributes, $numberOfPosts)
    {
        if (isset($attributes['selectedCategory']) && $attributes['selectedCategory'] != 0) {
            return $this->getPosts($attributes, $numberOfPosts);
        }
        return null;
    }

    public function register()
    {
        register_block_type('wpplugincontainer/gfcategorysidebar', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));
    }

    private function getPosts($attributes, $numberOfPosts)
    {
        global $restrictedPosts;
        $posts = [];

        $postsDb = $this->parseFixedPosts($attributes, $numberOfPosts);


        //When specific category is selected
        if ($attributes['selectedCategory'] !== '-1') {
            $categoryId = $attributes['selectedCategory'];
            $categoryName = $attributes['selectedCategoryName'];

            /*If user didnt input enough post ids for template to work
                     exclude posts from input and get newest for the remaining posts
                     */
            if (count($postsDb) < $numberOfPosts) {
                $postsDb = array_merge($postsDb, get_posts([
                    'category' => $categoryId,
                    'exclude' => $restrictedPosts,
                    'numberposts' => $numberOfPosts - count($postsDb),
                    'post_status'     => 'publish'
                ]));
            }

            foreach ($postsDb as $key => $post) {
                $restrictedPosts[] = $post->ID;
                $imageId = (get_post_thumbnail_id($post->ID)? : get_option("defaultFeaturedImage"));
                $posts[$key] = [
                    'categoryName' => $categoryName,
                    'categoryId' => $categoryId,
                    'categoryUrl' => str_replace('category/', '', get_category_link($categoryId)),
                    'url' => get_permalink($post->ID),
                    'postId' => $post->ID,
                    'imageId' => $imageId,
                    'postType' => $post->post_type,
                    'postTitle' => $post->post_title,
                    'postContent' => $post->post_content,
                    'publishDate' => $post->post_date
                ];

            }
        } else  /*When all categories are selected */ {

            /*If user didnt input enough post ids for template to work
                        exclude posts from input and get newest for the remaining posts
                        */
            if (count($postsDb) < $numberOfPosts) {
                $postsDb = array_merge($postsDb, get_posts([
                    'exclude' => $restrictedPosts,
                    'numberposts' => $numberOfPosts - count($postsDb),
                    'post_status'     => 'publish',
                    'category__not_in' => [55687]
                ]));
            }

            foreach ($postsDb as $key => $post) {
                $urls = [
                    'siprod.djavolak.info', 'srpska.greenfriends.systems', 'srpska.local'
                ];
                $restrictedPosts[] = $post->ID;
//                    $post['imageUrl'] = str_replace($urls, 'srpskainfo.com', get_the_post_thumbnail_url($post));
//                    $post['imageUrl'] = get_the_post_thumbnail_url($post, 'list-small');
                $categoryObject = get_the_category($post->ID);
                $imageId = (get_post_thumbnail_id($post->ID)? : get_option("defaultFeaturedImage"));
                $posts[$key] = [
                    'categoryName' => $categoryObject[0]->name,
                    'categoryId' => $categoryObject[0]->term_id,
                    'categoryUrl' => get_category_link($categoryObject),
                    'url' => get_permalink($post->ID),
                    'postId' => $post->ID,
                    'imageId' => $imageId,
                    'postType' => $post->post_type,
                    'postTitle' => $post->post_title,
                    'postContent' => $post->post_content,
                    'publishDate' => $post->post_date
                ];
            }
        }

        if (!isset($posts)) {
            return null;
        }

        return [
            'posts' => $posts,
            'selectedCategory' => $attributes['selectedCategory']
        ];
    }

    public function parseFixedPosts($attributes, $numberOfPosts)
    {
        // Prepare the array
        $postIds = $postsDb = [];
        // Set the current date
        $dtNow = new \DateTime('now', new \DateTimeZone('Europe/Sarajevo'));
        // Set the variables according the the number of posts
        for ( $i = 1; $i <= $numberOfPosts; $i ++ ) {
            $postIndex = 'post' . $i;
            $postExpiryIndex = 'post' . $i . 'Expiry';
            $postReplacementIndex = 'post' . $i . 'Replacement';
            // If there is user input for the fixed post
            if ($this->isPostIndexEmpty($attributes,$postIndex) === false) {
                // If the expiry index is set
                if ($this->isSetPostExpiry($attributes,$postExpiryIndex) === true) {
                    // Get the date of the expiry
                    $dt = new \DateTime($attributes[$postExpiryIndex], new \DateTimeZone('Europe/Sarajevo'));
                    // If the current datetime is greater than the expiry get the replacement index
                    if ($dtNow->getTimestamp() >= $dt->getTimestamp()) {
                        if ($this->isSetPostReplacement($attributes,$postReplacementIndex) === true) {
                            $postIds[] = $attributes[$postReplacementIndex];
                        }
                    } else { // If the current datetime is NOT greater than the expiry get the post index
                        $postIds[] = $attributes[$postIndex];
                    }
                } else { // If the expiry index is not set
                    $postIds[] = $attributes[$postIndex];
                }
            //If there is no user input for the fixed post but the expiry and the replacement is set
            } elseif ($this->isPostIndexEmpty($attributes, $postIndex) === true
                && $this->isSetPostExpiry($attributes, $postExpiryIndex) === true
                && $this->isSetPostReplacement($attributes, $postReplacementIndex) === true) {

                // Get the expiry index date
                $dt = new \DateTime($attributes[$postExpiryIndex], new \DateTimeZone('Europe/Sarajevo'));
                // If the current date is greater the expiry date get the replacement post index
                if ($dtNow->getTimestamp() >= $dt->getTimestamp()) {
                    $postIds[] = $attributes[$postReplacementIndex];
                }
            }
        }
        foreach ($postIds as $postId) {
            $post = get_post($postId);
            if ($post) {
                $postsDb[] = $post;
            }
        }

        return $postsDb;
    }

    public function prepareHtml($attributes)
    {
        ob_start();
        if(!isset($attributes['numberOfPosts']) || $attributes['numberOfPosts'] === '' || $attributes['numberOfPosts'] === '-') {
            $attributes['numberOfPosts'] = '5';
        }

        $dtNow = new \DateTime('now', new \DateTimeZone('Europe/Sarajevo'));
        $data = $this->getPostsForTemplate($attributes, $attributes['numberOfPosts']);
        $multipleAuthors = new MultipleAuthors();
        if ($data) {
            $data['multipleAuthors'] = $multipleAuthors;
	        if ($this->isMobileApp($_SERVER)) {
		        GfShopThemePlugins::getTemplatePartials('gutenberg', 'categorySidebar', 'sidebarTemplateMobileApp', $data);
		        return ob_get_clean();
	        }
            if ($this->isMobile) {
                GfShopThemePlugins::getTemplatePartials('gutenberg', 'categorySidebar', 'sidebarTemplateMobile', $data);
                return ob_get_clean();
            }
            GfShopThemePlugins::getTemplatePartials('gutenberg', 'categorySidebar', 'sidebarTemplate', $data);
            return ob_get_clean();
        }
        return false;
    }

}