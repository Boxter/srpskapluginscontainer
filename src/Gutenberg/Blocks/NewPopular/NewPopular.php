<?php

namespace GfWpPluginContainer\Gutenberg\Blocks\NewPopular;

use GfWpPluginContainer\Gutenberg\Blocks\BaseBlock\AbstractBlock;
use GfWpPluginContainer\Wp\PostHelper;


class NewPopular extends AbstractBlock
{

    public function register()
    {
        register_block_type('wpplugincontainer/gfnewpopular', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));
    }

    public function prepareHtml($attributes)
    {

        //Default number of posts
        $numberOfPosts = '5';
        if (isset($attributes['numberOfPosts']) && strlen($attributes['numberOfPosts']) !== 0){
            $numberOfPosts = $attributes['numberOfPosts'];
        }

	    $newest = PostHelper::getNewestPosts($numberOfPosts);
        $popular = PostHelper::getPopularPosts($numberOfPosts);
		$newestNewsPageUrl = get_permalink(get_page_by_path('sve-vijesti')->ID);
	    $popularNewsPageUrl = get_permalink(get_page_by_path('popularno')->ID);

	    if ($this->isMobileApp($_SERVER)) {
		    $popularNewsPageUrl = parseAppUrl('page',$popularNewsPageUrl);
		    $newestNewsPageUrl  = parseAppUrl('page',$newestNewsPageUrl);
            $html = include(PLUGIN_DIR . 'templates/gutenberg/NewPopular/NewPopularMobile.phtml');
            return $html;
	    }
        $html = include(PLUGIN_DIR . 'templates/gutenberg/NewPopular/NewPopular.phtml');
        return $html;
    }

    private function printItem($timeAgo, $url, $title)
    {
        if ($this->isMobileApp($_SERVER))
        {
            $url = parseAppUrl('article',$url);
        }
        $escapedTitle = esc_attr($title);
        $html = sprintf('<li>
                            <div class="news__time">
                                <span>%d</span>
                                <span>%s</span>
                            </div>
                            <h3><a title="%s" href="%s">%s</a></h3>
                        </li>', $timeAgo[0], $timeAgo[1], $escapedTitle, $url, $title);
        return $html;
    }
}