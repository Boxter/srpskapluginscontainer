<?php


namespace GfWpPluginContainer\Gutenberg\Blocks\Banner;

class BannerBlock
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public function register()
    {
        register_block_type('wpplugincontainer/gfbannerblock', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));
    }

    public function render($attributes)
    {
    /*   Inputs: fixed, sticky, sticky area.
         Fixed and sticky have the same layout with different classes,
         sticky area has a different layout with different classes.
    */
        if (wp_is_mobile()) {
            return null;
        }

        // Default class for desktop (mind the space after the class name ends)
        $class = 'banner banner--desktop ';

        // If fixed is selected append to $class
        if($attributes['selectedType'] === '0') {
            $class .= 'banner__top ';
        }


        // If sticky is selected append to $class
        if ($attributes['selectedType'] === '1') {
            $class .= 'sticky ';
        }

        // If sticky area is selected append to $class
        if ($attributes['selectedType'] === '2') {
            $class .= 'banner--space-bottom sticky ';
        }
        // If content is passed in the input field
        if (isset($attributes['content'])){
            if($attributes['selectedType'] === '2') {
                return sprintf('<div class="sticky__area"><div class="%s">%s</div></div>', $class, $attributes['content']);
            }
            else {
                return sprintf('<div class="%s">%s</div>', $class, $attributes['content']);
            }
        }
        return null;
    }
}