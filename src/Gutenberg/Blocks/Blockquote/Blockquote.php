<?php


namespace GfWpPluginContainer\Gutenberg\Blocks\Blockquote;


class Blockquote
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public function register()
    {
        register_block_type('wpplugincontainer/blockquote', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));

    }

    public function render($attributes)
    {
        return sprintf("<blockquote>
                            <p>'%s'</p>
                        </blockquote>",$attributes['quote']);
    }
}