<?php


namespace GfWpPluginContainer\Gutenberg\Blocks\BlicBlock;


class BlicBlock {
	public function __construct() {
		add_action( 'init', [ $this, 'register' ] );
		add_action('init' , [$this, 'createBlicPage']);
	}

	public function register() {
		register_block_type( 'wpplugincontainer/gfblicblock', array(
			'editor_script'   => 'gfBlocks',
			'editor_style'    => 'gfBlocksEditor',
			'style'           => 'gfBlocksFront',
			'render_callback' => [ $this, 'render' ]
		) );

	}

	public function createBlicPage() {
		$page = get_page_by_title( 'Blic Naslovna', 'OBJECT', 'page' );

		if ( empty( $page ) ) {
			$pageId = wp_insert_post(
				[
					'comment_status' => 'close',
					'ping_status'    => 'close',
					'post_author'    => 1,
					'post_title'     => 'Blic Naslovna',
					'post_status'    => 'publish',
					'post_type'      => 'page',
				]
			);
			if (!$pageId) {
				wp_die('Error creating template page');
			} else {
				update_post_meta($pageId, '_wp_page_template',  'blicNaslovna.php');
			}
		}
	}

	public function render( $attributes, $content ) {
	    // Print this block only on desktop
        /* image doesnt render until the homepage is saved, so using gfBlock object data is invalid (for now)
            see wp-content\plugins\srpskapluginscontainer\src\Gutenberg\Gutenberg.php enqueue_block_assets  */
	    if(!wp_is_mobile() && is_front_page()) {
            $blicNaslovnaPage = get_page_by_title('blic naslovna');
            $pageLink = get_permalink($blicNaslovnaPage->ID);
            $imgUrl = get_option('blicNaslovnaImg');
            $image = '<a href="%s"> <img title="blic naslovna" alt="blic naslovna" src="%s"></a>';
            $html = sprintf('<div class="euroblic">' . $image .
                '<span><strong>Euroblic</strong> naslovna</span>
                    </div>', $pageLink, $imgUrl);

            return $html;
        }
	}
}