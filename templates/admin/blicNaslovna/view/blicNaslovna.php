<?php
$option = get_option('blicNaslovnaImg');
$imgId = $option ? : 0;
$buttonText = strlen($imageUrl) > 0 ? 'Izmenite sliku' : 'Ubacite sliku';
?>


<form method="post" action="options.php">
    <?php
    settings_fields('blicNaslovnaImgs');
    ?>
<h2>Izaberite Euroblic naslovnu</h2>
    <div>
        <input id="naslovnaImg" type="hidden" name="blicNaslovnaImg" value="<?= $imgId ?>" />
        <input id="imgUploadButton" type="button" class="button-primary" value="<?= $buttonText ?>" />
    </div>
<div>
    <?php if ($imgId !== 0):?>
    <img id="naslovnaPreview" src="<?= wp_get_attachment_image_url($imgId)?>" alt="blic naslovna">
    <?php endif; ?>
</div>
    <?php submit_button() ?>
</form>