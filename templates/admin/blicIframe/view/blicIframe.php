<?php
if (!current_user_can('manage_options')) {
	wp_die(__('You do not have sufficient permissions to access this page.'));
}

settings_errors();

$options = get_option('blicIframeIds');
$post1 = $options[1] ?: '';
$post2 = $options[2] ?: '';
$post3 = $options[3] ?: '';
$post4 = $options[4] ?: '';
$post5 = $options[5] ?: '';
?>
<div class="wrap">
	<form method="post" action="options.php">
		<?php
		settings_fields('blicIframeIds');
		?>
		<h1><?= __('Blic Iframe', 'gfShopTheme') ?></h1>

		<h4><?= __('U polja ispod unesite 5 ID-ova članaka koji će se prikazivati na blic.rs stranici (iframe).', 'gfShopTheme') ?></h4>
		<div>
            <div>
                <label>Članak 1</label>
                <input type="number" name="blicIframeIds[1]" value="<?=$post1?>">
            </div>
            <div>
                <label>Članak 2</label>
                <input type="number" name="blicIframeIds[2]" value="<?=$post2?>">
            </div>
            <div>
                <label>Članak 3</label>
                <input type="number" name="blicIframeIds[3]" value="<?=$post3?>">
            </div>
            <div>
                <label>Članak 4</label>
                <input type="number" name="blicIframeIds[4]" value="<?=$post4?>">
            </div>
            <div>
                <label>Članak 5</label>
                <input type="number" name="blicIframeIds[5]" value="<?=$post5?>">
            </div>
		</div>

		<?php submit_button(); ?>
	</form>
</div>
