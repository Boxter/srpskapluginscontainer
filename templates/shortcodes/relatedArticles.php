<?php
// If amp load fa icons, else load fas
$iconClass = '';
if(function_exists( 'is_amp_endpoint' ) && is_amp_endpoint()){
    $iconClass = 'fa';
}
else {
    $iconClass = 'fas';
}
$postIds = $data['ids'];
$postIds = explode(',', $postIds);
$html = '<div class="article__related"> <h3>Pročitajte još</h3><ul>';
$isApp = $_SERVER['REQUEST_URI'] === '/mobile-home/' || strpos( $_SERVER['REQUEST_URI'], '/app/' ) !== false;
foreach ($postIds as $postId) {
	$permalink = $isApp? parseAppUrl('article',get_permalink($postId)):get_permalink($postId);
	if (strlen(trim($postId)) != 0) {
		$post = get_post($postId);
		$html .= sprintf('  <li><i class="%s fa-chevron-right"></i><a href="%s">%s</a></li>',
			$iconClass, $permalink, $post->post_title);
	}
}
$html .= '</ul></div>';

echo $html;