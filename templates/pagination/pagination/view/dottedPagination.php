<?php
$page = (int) $data['page'];
$firstPageClass = '';
$lastPageClass = '';
$lastPage = (int) $data['lastPage'];
if ($page === 1) {
    $firstPageClass = 'active';
}
if ($page === $lastPage) {
    $lastPageClass = 'active';
}
?>
<ul>
    <?php if ($lastPage > 4 && $lastPage !== 1) { ?>
        <li class="prevButton"><?= get_previous_posts_link('<i class="fas fa-arrow-left "></i>') ?></li>
        <?php if ($page !== 1): ?>
            <li class="<?= $firstPageClass ?>"><a href="<?= get_pagenum_link(1) ?>">1</a></li>
        <?php endif ?>
        <?php if ($page > 4): ?>
            <li><span>...</span></li>
        <?php endif ?>
        <?php if ($page <= 4): ?>
            <?php if ($page === 4): ?>
                <li class=""><a href="<?= get_pagenum_link($page - 2) ?>"><?= $page - 2 ?></a></li>
            <?php endif; ?>
            <?php if ($page > 2): ?>
                <li class=""><a href="<?= get_pagenum_link($page - 1) ?>"><?= $page - 1 ?></a></li>
            <?php endif; ?>
            <?php if (!wp_is_mobile()): ?>
                <li class="active"><a href="<?= get_pagenum_link($page) ?>"><?= $page ?></a></li>
                <li class=""><a href="<?= get_pagenum_link($page + 1) ?>"><?= $page + 1 ?></a></li>
                <li class=""><a href="<?= get_pagenum_link($page + 2) ?>"><?= $page + 2 ?></a></li>
            <?php else: ?>
                <li class=""><a href="<?= get_pagenum_link($page - 1) ?>"><?= $page - 1 ?></a></li>
                <li class="active"><a href="<?= get_pagenum_link($page + 1) ?>"><?= $page + 1 ?></a></li>
                <li class=""><a href="<?= get_pagenum_link($page + 2) ?>"><?= $page + 2 ?></a></li>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ($page > 4 && $page < $lastPage - 3): ?>
            <li class=""><a href="<?= get_pagenum_link($page - 1) ?>"><?= $page - 1 ?></a></li>
            <li class="active"><a href="<?= get_pagenum_link($page) ?>"><?= $page ?></a></li>
            <li class=""><a href="<?= get_pagenum_link($page + 1) ?>"><?= $page + 1 ?></a></li>
        <?php endif ?>

        <?php if ($page >= $lastPage - 3): ?>
            <?php if ($page == $lastPage): ?>
                <li class=""><a href="<?= get_pagenum_link($page - 3) ?>"><?= $page - 3 ?></a></li>
                <li class=""><a href="<?= get_pagenum_link($page - 2) ?>"><?= $page - 2 ?></a></li>
            <?php endif ?>
            <li class=""><a href="<?= get_pagenum_link($page - 1) ?>"><?= $page - 1 ?></a></li>
            <li class="active"><a href="<?= get_pagenum_link($page) ?>"><?= $page ?></a></li>
            <?php if ($page < $lastPage - 1): ?>
                <li class=""><a href="<?= get_pagenum_link($page + 1) ?>"><?= $page + 1 ?></a></li>
            <?php endif ?>

            <?php if ($page == $lastPage - 3): ?>
                <li class=""><a href="<?= get_pagenum_link($page + 2) ?>"><?= $page + 2 ?></a></li>
            <?php endif ?>
        <?php endif; ?>

        <?php if ($page < ($lastPage - 3)): ?>
            <li><span>...</span></li>
        <?php endif ?>

        <?php if ($page !== $lastPage): ?>
            <li class="<?=$lastPageClass?>"><a href="<?= get_pagenum_link($lastPage) ?>"><?= $lastPage ?></a></li>
            <li class="nextButton"><a href="<?= get_pagenum_link($page + 1) ?>"><i class="fas fa-arrow-right "></i></a></li>
        <?php endif ?>

    <?php } else {
        for ($i = 1; $i <= $lastPage; $i++){
             echo '<li';
             if ($page == $i) {
                echo ' class="active"';
             }
             echo '><a href="'. get_pagenum_link($i) .'">' . $i . '</a></li>';
        }
    } ?>
</ul>
