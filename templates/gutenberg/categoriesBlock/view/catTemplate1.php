<?php if($data['selectedCategory'] != '-1') :?>
<h3 class="box__title"><a href="<?=$data['posts'][0]['categoryUrl']; ?>" title="<?=($data['posts'][0]['categoryName']); ?>"><?=($data['posts'][0]['categoryName']); ?></a></h3>
<?php endif; ?>
<section class="news items__5">
    <?php
        $i = 1;
        foreach($data['posts'] as $post):
            $post['preContent'] = $post['postContent'];
            if ($i < 3 ) {
                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'list-big'));
                $post['imageWidth'] = 427;
                $post['imageHeight'] = 285;
            } else {
                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'list-small'));
                $post['imageWidth'] = 275;
                $post['imageHeight'] = 188;
            }

            // If all categories options is selected, override individual category url
            if ($data['selectedCategory'] === '-1') {
                $post['categoryUrl'] = str_replace('category/', '', get_category_link($post['categoryId']));
            }
            include('articleItem.phtml');
        $i++; endforeach; ?>
</section>
