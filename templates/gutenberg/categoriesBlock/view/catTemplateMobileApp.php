<?php use Carbon\Carbon;
Carbon::setLocale('bs');
if($data['selectedCategory'] != '-1') :
//    $catLink = $data['posts'][0]['categoryUrl'] . '?isapp=true';
    $catLink = str_replace('/category','', $data['posts'][0]['categoryUrl']);
    ?>
    <h3  class="box__title">
        <a href="<?=parseAppUrl('page', $catLink)?>" title="<?=esc_attr($data['posts'][0]['categoryName'])?>"><?=($data['posts'][0]['categoryName']) ?></a>
    </h3>
<?php endif; ?>
<section class="news items__5">
    <?php
    $i = 1;

    foreach($data['posts'] as $post):
        $postDate = new Carbon($post['publishDate'], new \DateTimeZone('Europe/Sarajevo'));
        $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());

        $authorName= $data['multipleAuthors']->getOwnersForPost($post['postId'])[0]->getAuthorDisplayName();
        $authorUrl = get_author_posts_url($data['multipleAuthors']->getOwnersForPost($post['postId'])[0]->getAuthorId());

        if ($data['selectedCategory'] === '-1') {
            $post['categoryUrl'] = str_replace('/category','', get_category_link($post['categoryId']));
        }

        if ($i === 1) {
            $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'large'));
            $post['imageWidth'] = 374;
            $post['imageHeight'] = 250;
        } else {
            $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'medium'));
            $post['imageWidth'] = 120;
            $post['imageHeight'] = 82;
        }
        ?>
        <article class="news__item">
            <a href="<?=parseAppUrl('article', $post['url'])?>" title="<?=esc_attr($post['postTitle'])?>">
                <figure><img src="<?= $post['imageUrl']?>" height="<?=$post['imageHeight']?>" width="<?=$post['imageWidth']?>" alt="<?= esc_attr($post['postTitle']) ?>"/><?php if ($post['postType'] === 'video') {
                        echo '<span class="icon"><i class="fa fa-play"></i></span>';
                    } elseif ($post['postType'] === 'audio') {
                        echo '<span class="icon"><i class="fa fa-microphone"></i></span>';
                    }?></figure>
            </a>
            <div>
                <span class="news__category">
                    <span class="categoryAuthor">
                        <?php if (mb_strtoupper($post['categoryName'])=='KOLUMNE' ) : ?>
                            <a  title="<?=$authorName?>" href="<?=parseAppUrl('author', $authorUrl)?>"><?=$authorName?></a>
                        <?php else :?>
                            <a title="<?=$post['categoryName']?>" href="<?=parseAppUrl('page', $post['categoryUrl'])?>"><?=$post['categoryName']?></a>
                        <?php endif;?>
                    </span>
                    <span class="postDate"><i class="fa fa-clock"></i> <?=$timeAgo[0]?> <?=$timeAgo[1]?></span>
                </span>
                <h1><a href="<?=parseAppUrl('article', $post['url'])?>" title="<?=esc_attr($post['postTitle'])?>"><?=$post['postTitle'] ?></a></h1>
            </div>
        </article>
        <?php $i++; endforeach;?>
</section>
