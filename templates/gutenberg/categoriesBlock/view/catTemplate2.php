<?php if($data['selectedCategory'] != '-1') : ?>
<h3 class="box__title"><a href="<?=$data['posts'][0]['categoryUrl']; ?>" title="<?=($data['posts'][0]['categoryName']); ?>"><?=($data['posts'][0]['categoryName']); ?></a></h3>
<?php endif; ?>
<section class="news items__2">
    <?php
    $i = 1;
    foreach($data['posts'] as $post):
        $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'list-big'));
        $post['imageWidth'] = 427;
        $post['imageHeight'] = 285;
        include('articleItem.phtml');
        $i++;
    endforeach; ?>
</section>