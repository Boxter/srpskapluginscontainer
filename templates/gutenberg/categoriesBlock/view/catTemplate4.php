<?php if($data['selectedCategory'] != '-1') : ?>
<h3 class="box__title"><a href="<?=$data['posts'][0]['categoryUrl']; ?>" title="<?=($data['posts'][0]['categoryName']); ?>"><?=$data['posts'][0]['categoryName']; ?></a></h3>
<?php endif; ?>
<section class="news items__3">
    <?php
    foreach($data['posts'] as $post):
        $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'list-small'));
        $post['imageWidth'] = 275;
        $post['imageHeight'] = 188;
        include('articleItem.phtml');
    endforeach; ?>
</section>
